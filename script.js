/* --------------------------------------------------- */
/* Script : jquery/javascript/ajax/json client wew :) */
/* ------------------------------------------------- */

//global variables******************
		lat=42.6976;
		lng=2.8954;
		radius=5635;
		latitude =	43.99;
		longitude = 3.01;
		influence= 3;
//**********************************

//affichage dynamique du background----------------------------------------------------------------------------------------------
$(document).ready(function(){

	var cnt=0, bg;
	var $body = $('body');
	var arr = ['images/lemons_backround.jpg','images/lemons_backround1.jpg','images/lemons_backround2.jpg'];

	var bgrotater = setInterval(function() {
		if (cnt==3) cnt=0;
		bg = 'url("' + arr[cnt] + '")';
		cnt++;
		$body.css('background-image', bg);
	}, 7000);

	
	function stopColor() {
		clearInterval(bgrotater);
	}
	
// GET weather and timestamp----------------------------------------------------------------------------------------------------
setInterval(ajaxCall, 3000); //appelle la fonction toutes les 3000 millisecondes pour mettre a jour la meteo
function ajaxCall() {
$.ajax({
	url: "http://obscure-tundra-53830.herokuapp.com/metrology",
	type: 'GET',
	dataType: 'json', 
	success: function weather(res) {
		
										//console.log(res);
										$("#today_weather").text(res.weather[0].weather)
										$("#tomorrow_weather").text(res.weather[1].weather)
										$("#timestamps").text(res.timestamp);
						//switch weather image 
						var today_src_img = document.getElementById("today_weather_img");
						var tomorrow_src_img = document.getElementById("tomorrow_weather_img");

						var today_weather = res.weather[0].weather;
						var tomorrow_weather = res.weather[1].weather;
						//console.log(today_weather);
						//console.log(tomorrow_weather);
						switch(today_weather){

							case  "sunny":
							today_src_img.src="images/sunny.png";
							break;
							case  "cloudy":
							today_src_img.src="images/cloudy.png";
							break;
							case  "thunder":
							today_src_img.src="images/thunder.png";
							break;
							case  "rainy":
							today_src_img.src="images/rainy.png";
							break;
							case  "heatwave":
							today_src_img.src="images/heatwave.png";
							break;		}

							switch(tomorrow_weather){

								case  "sunny":
								tomorrow_src_img.src="images/sunny.png";
								break;
								case  "cloudy":
								tomorrow_src_img.src="images/cloudy.png";
								break;
								case  "thunder":
								tomorrow_src_img.src="images/thunder.png";
								break;
								case  "rainy":
								tomorrow_src_img.src="images/rainy.png";
								break;
								case  "heatwave":
								tomorrow_src_img.src="images/heatwave.png";
								break;
							}
						//calculate date and time
						var timestamps = res.timestamp;		
						var	time = timestamps % 24 ;
						var day = (timestamps - time) /24;
						$('#day').html(day+1);
						$('#time').html(time+"hours");

					},      
					error: function() { 
						alert("Il semble que vous ayez perdu..."); 
					} 				

				});
}
var element1='<td></td>';

//GET /RESET----------------------------------------------------
//$(document).ready(function(){
	$("#exit_button").click(function(e) {
		e.preventDefault();
		var nom = $("#name_response").text();
		$.ajax({
			type: 'DELETE',
			url: "http://obscure-tundra-53830.herokuapp.com/player/"+nom,	
			success: function(res) {
				$("#exit_response").append(res.suppression);
				console.log(res.suppression);
				console.log($("#name_response").text());
			}	
		});
	});		
//});

//---------Post: start game (player name / player location)----
var nom;

$("#rejoindre_partie").click(function(e) {
	e.preventDefault();
	var nameis = $("#player_name").val() ;

	$.ajax({
		type: "POST",
		url: "http://obscure-tundra-53830.herokuapp.com/players",
		data:  JSON.stringify({"name": nameis}),
		contentType: "application/json",
		dataType:"json",
		success: function(msg){
							document.getElementById("name_response").innerHTML = msg.name;
							nom=msg.name;
							getmap();
						},
						
						failure: function(errMsg) {
							alert(errMsg);   
						}

					});

// GET /map/player----------------------------------------------


		function getmap(){//execter les infos player aprés avoir rejoindre la partie
			$.ajax({
				url: "http://obscure-tundra-53830.herokuapp.com/map/"+nom,
				type: 'GET',
				dataType: 'json', 
				success: function(res) {
					var maxi = Object.keys(res["playerInfo"]["drinksOffered"]).length;
		//alert(maxi);
		//console.log(res["playerInfo"]["profit"]);
		$("#Budgets").append(res["playerInfo"]["cash"]);
		$("#Total_profit").append(res["playerInfo"]["profit"]);
		$("#total_sales").append(res["playerInfo"]["sales"]);
		//console.log(maxi);
		for(var i = 0; i <= maxi; i++){						
			$("#table_data").append('<tr id="boissons"><td>' +res["playerInfo"]["drinksOffered"][i]["name"] + '</td><td>' + res["playerInfo"]["drinksOffered"][i]["price"]+ '</td>'+'</tr>');
			$("#action_creer_recette").append('<tr id="action_boissons'+i+'"><td id="nom_boissons'+i+'">' + res["playerInfo"]["drinksOffered"][i]["name"]+ '</td><td id="qt_boissons'+i+'">' + '<input type="text" id="checkbox1"></input>'+ '</td><td id="prix_boissons'+i+'">'+'<input type="text" id="checkbox1"></input></td></tr>');		
			document.getElementById("latitude").innerHTML = res["map"]["itemsByPlayer"][nom]["0"]["location"]["latitude"];
			document.getElementById("longitude").innerHTML = res["map"]["itemsByPlayer"][nom]["0"]["location"]["longitude"];
							lat = res["map"]["itemsByPlayer"][nom]["0"]["location"]["latitude"];
							lng = res["map"]["itemsByPlayer"][nom]["0"]["location"]["longitude"];
							radius = res.influence;
							console.log(lat);
							console.log(lng);
		}										
				}		//success
			});		//ajax

		}//getmap

});//button rejoindre

//GET ingredients informations----------------------------------
$.ajax({
	url: "http://obscure-tundra-53830.herokuapp.com/ingredients",
	type: 'GET',
	dataType: 'json', 
	success: function(res) {
		var maxi = Object.keys(res.ingredient).length;
		for(var i = 0; i < maxi; i++){
				//console.log(res.ingredients[0].name);							
				$("#ingredients_info").append('<label id="label1">'+'<input type="checkbox" id="checkbox1"><label id="ingredient_name">'+res.ingredient[i].name+'</label><label id="ingredient_prix">'+res.ingredient[i].cost+"€"+'</label></input></label>');
				
			}
		}
		
	});


//POST actions-R6----------------------------------------------

//valider l'envoit des actions
var selected = new Object();
var prepare = new Array();
var price = new Array();
var action = new Object();
$("#sent_actions").click(function(e) {
//	e.preventDefault();		

	//$('input[id="checkbox1"]:checked').each(function() {
		//if(this.name == "drinks"){
				//Action preparer boisson------------------------------------------------		
				var count = $("#action_creer_recette tbody").children().length;	
				var prepare_value=new Array();
				var price_value	=new Array();
				var recipe_name = new Array();
				var prepare_response = {};
				var price_response = {};
				var recipe = {};
				var ingredients=new Array();
				for(var i=0; i<count-1; i++){

								recipe_name[i] = $("#nom_boissons"+i).text();
								prepare_value[i] = Number($("#qt_boissons"+i+" input").val());
								price_value[i] = Number($("#prix_boissons"+i+" input").val());
									//console.log("********");								
								prepare_response[recipe_name[i]] = prepare_value[i];
								//console.log(prepare_response);								
								price_response[recipe_name[i]] = price_value[i];																														
									}

								latitude =	Number($("#Latitude_input ").val());
								longitude = Number($("#Longitude_input ").val());
								influence = Number($("#prix_input ").val());
															
								action = [{ 
									"kind":"recipe" ,
								    "recipe":{
										  	"name":"bizzare",//$("#recipe_name").val(),
										  	"ingredients":[{
										  		"name":"cafe",
										  		"cost":1,
										  		"hasAlcohol": false,
										  		"isCold": true,
										  	}],
										  	"hasAlcohol": false,
										  	"isCold": false,
										     } 
										    },
									
									{ "kind": "ad", 
									"location": { 
										"latitude" : 26.0,
										 "longitude": 24.9 }, 
										 "influence": 2 },
									{ 
									"kind":"drinks",
									"prepare": prepare_response,
									"price": price_response	
										}];							   								
									selected = {
											action,
											"simulated":"false"
										};

			//console.log(JSON.stringify(selected));						
	$.ajax({
		type: "POST",
		url: "http://obscure-tundra-53830.herokuapp.com/action/Parayre",
		data:  JSON.stringify(selected),
		contentType: "application/json",
		dataType:"json",
		success: function(msg){
							$("#prix_label").text(msg.cost+"€");
							$("#fonds_label").text(msg.success);
							
							//console.log(msg);
							//console.log(document.getElementById("fonds_label").innerHTML);
							//si simuleted ==true on place le nouveau panneau sur la map
							if(document.getElementById("fonds_label").innerHTML == "true"){							
									add_panneau(latitude,longitude,radius);
							}
							else{
								alert("impossible d'effectuer l'action");
							}
						},
						
						failure: function() {
							alert("errMsg");   
						}

					});
		
});


});//doc


//--------------------------Display Map------------------------
	var map;
function initMap() {
	
	var uluru = {lat: lat, lng:lng};
	var myCenter= {lat: 658, lng:68};
    map = new google.maps.Map(document.getElementById('map'), {
		zoom: 12,
		center: uluru
	});
	var marker = new google.maps.Marker({
		position: myCenter,
		icon:'images/panneau.png' });
	marker.setMap(map);
	var marker = new google.maps.Marker({
		position: uluru,
		icon:'images/lemon-icon.png',
		map: map
	});
		// Create the legend and display on the map
		var legend = document.createElement('div');
		legend.id = 'legend';
		var content = [];
		content.push('<h3>Legendes: </h3>');
		content.push('<p><img src="images/panneau.png" id="legend_img" /> Panneau de pub</p>');
		content.push('<p><img src="images/lemon-icon.png" id="legend_img" /> Stand</p>');
		legend.innerHTML = content.join('');
		legend.index = 1;
		map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
		

		google.maps.event.addDomListener(window, 'load', initialize);


	}

//--------------------------fonction qui ajoute un panneau à la map:appeler dans /action/player  -----------------------------

function add_panneau(lat,longitude,radius){

	var myCenter= {lat:lat, lng:longitude};
		var marker = new google.maps.Marker({
		position: myCenter,
		icon:'images/panneau.png' });
	marker.setMap(map);

var circle = new google.maps.Circle({
  map: map,
  radius: radius,    // 10 miles in metres
  fillColor: '#AA0000'
});
circle.bindTo('center', marker, 'position');
}




